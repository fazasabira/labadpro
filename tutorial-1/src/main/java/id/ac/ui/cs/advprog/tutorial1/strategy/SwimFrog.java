package id.ac.ui.cs.advprog.tutorial1.strategy;

public class SwimFrog implements SwimBehav {

    @Override
    public void swim() {
        System.out.println("Berenang gaya katak");
    }
}
