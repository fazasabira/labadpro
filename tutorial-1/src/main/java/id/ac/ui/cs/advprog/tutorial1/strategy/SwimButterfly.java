package id.ac.ui.cs.advprog.tutorial1.strategy;

public class SwimButterfly implements SwimBehav {
    @Override
    public void swim() {
        System.out.println("Berenang gaya kupu - kupu");
    }
}
