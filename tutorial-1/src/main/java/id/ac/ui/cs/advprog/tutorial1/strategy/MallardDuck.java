package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    // TODO Complete me!
    MallardDuck(){
        setFlyBehavior(new FlyNoWay());
        setQuackBehavior(new MuteQuack());

    }

    @Override
    public void display() {
        System.out.println("I'm Mallard duck");
    }
}
