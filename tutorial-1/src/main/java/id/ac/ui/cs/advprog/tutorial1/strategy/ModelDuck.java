package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
    // TODO Complete me!
    ModelDuck(){
        setFlyBehavior(new FlyRocketPowered());
        setQuackBehavior(new Quack());

    }

    @Override
    public void display() {
        System.out.println("I'm model duck");
    }
}
